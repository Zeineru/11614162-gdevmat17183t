﻿using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private List<Movable> shapeList = new List<Movable>();

        public Vector3 pushingForce = new Vector3(0.05f, 0, 0);
        public Vector3 gravity = new Vector3(0, -0.05f, 0);


        public MainWindow()
        {
            for (int i = 0; i < 8; i++)
            {
                createProtons();
            }

            for (int i = 0; i < 8; i++)
            {
                createElectrons();
            }
        }

        #region INIZIALIZATION

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        Cube CreateNeutron(OpenGL gl)
        {
            Cube neutron = new Cube();

            neutron.objectType = "Neutron";

            neutron.color.r = 0.0f / 255.0f;
            neutron.color.g = 255.0f / 255.0f;
            neutron.color.b = 0.0f / 255.0f;
            neutron.color.a = 1.0f;

            neutron.mass = 10.0f;

            neutron.Scale = new Vector3(5.0f, 5.0f, 5.0f);

            neutron.Position.x = 0.0f;
            neutron.Position.y = 0.0f;

            neutron.Render(gl);

            return neutron;
        }

        void createProtons()
        {
            Cube newProton = new Cube();

            newProton.objectType = "Proton";

            float sizeScale = RandomNumberGenerator.GenerateFloat(0.5f, 1.0f);

            newProton.color.r = 240.0f / 255.0f;
            newProton.color.g = 55.0f / 255.0f;
            newProton.color.b = 25.0f / 255.0f;
            newProton.color.a = 1.0f;

            newProton.mass = RandomNumberGenerator.GenerateFloat(1.0f, 5.0f);

            newProton.Scale = new Vector3(sizeScale, sizeScale, 0);

            newProton.Position.x = RandomNumberGenerator.GenerateFloat(-1.0f, -40.0f);
            newProton.Position.y = RandomNumberGenerator.GenerateFloat(1.0f, 40.0f);

            shapeList.Add(newProton);
        }

        void createElectrons()
        {
            Cube newElectron = new Cube();

            newElectron.objectType = "Electron";

            float sizeScale = RandomNumberGenerator.GenerateFloat(0.5f, 1.0f);

            newElectron.color.r = 96.0f / 255.0f;
            newElectron.color.g = 135.0f / 255.0f;
            newElectron.color.b = 236.0f / 255.0f;
            newElectron.color.a = 1.0f;

            newElectron.mass = RandomNumberGenerator.GenerateFloat(1.0f, 5.0f);

            newElectron.Scale = new Vector3(sizeScale, sizeScale, 0);

            newElectron.Position.x = RandomNumberGenerator.GenerateFloat(-1.0f, -40.0f);
            newElectron.Position.y = RandomNumberGenerator.GenerateFloat(1.0f, 40.0f);

            shapeList.Add(newElectron);
        }

        void checkBounds(Movable shape)
        {
            if (shape.Position.x <= -60)
            {
                shape.Position.x = -60;
                shape.Velocity.x *= -1;
            }

            if (shape.Position.x >= 60)
            {
                shape.Position.x = 60;
                shape.Velocity.x *= -1;
            }

            if (shape.Position.y <= -30)
            {
                shape.Position.y = -30;
                shape.Velocity.y *= -1;
            }

            if (shape.Position.y >= 30)
            {
                shape.Position.y = 30;
                shape.Velocity.y *= -1;
            }
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            //gl.Clear(OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            #endregion

            Cube neutron = CreateNeutron(gl);

            foreach (Cube a in shapeList)
            {
                //a.ApplyForce(gravity);
                //a.ApplyForce(pushingForce);

                a.Render(gl);

                foreach (Cube b in shapeList)
                {
                    a.ApplyForce((neutron.CalculateAttraction(a)));

                    if (a != b)
                    {
                        if (a.HasCollidedWith(b))
                        {
                            a.Reflect(b);
                        }

                        if (a.objectType != b.objectType)
                        {
                            a.Attract(b);
                        }

                        else if (a.objectType == b.objectType)
                        {
                           a.Repel(b);
                        }
                    }

                }

                checkBounds(a);
            }
        }

        #region MOUSE FUNCTIONS
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
        }
        #endregion
    }
}
