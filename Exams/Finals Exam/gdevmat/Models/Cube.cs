﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;

namespace gdevmat.Models
{
    public class Cube : Movable
    {
        public Vector3 Scale = new Vector3(0.5f, 0.5f, 0.5f);

        public Cube()
        {

        }

        public Cube(Vector3 initPos)
        {
            this.Position = initPos;
        }

        public Cube(float x, float y, float z)
        {
            this.Position.x = x;
            this.Position.y = y;
            this.Position.z = z;
        }

        public override void Render(OpenGL gl)
        {
            base.Render(gl);
            gl.Begin(OpenGL.GL_TRIANGLE_STRIP);
            //Front face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            //Right face
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            //Back face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            //Left face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.End();
            gl.Begin(OpenGL.GL_TRIANGLE_STRIP);
            //Top face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.End();
            gl.Begin(OpenGL.GL_TRIANGLE_STRIP);
            //Bottom face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            gl.End();
        }

        public bool HasCollidedWith(Movable target)
        {
            bool xHasNotCollided =
            this.Position.x - this.Scale.x > target.Position.x + target.Scale.x ||
            this.Position.x + this.Scale.x < target.Position.x - target.Scale.x;

            bool yHasNotCollided =
            this.Position.y - this.Scale.y > target.Position.y + target.Scale.y ||
            this.Position.y + this.Scale.y < target.Position.y - target.Scale.y;

            bool zHasNotCollided =
            this.Position.z - this.Scale.z > target.Position.z + target.Scale.z ||
            this.Position.z + this.Scale.z < target.Position.z - target.Scale.z;

            return !(xHasNotCollided || yHasNotCollided || zHasNotCollided);
        }

        public void applyReactions(Movable target)
        {
            if (this.HasCollidedWith(target) == true && this.objectType == target.objectType)
            {
                this.Repel(target);
            }

            else if (this.HasCollidedWith(target) == true && this.objectType != target.objectType)
            {
                this.Attract(target);
            }
        }
    }
}
