﻿using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat.Models
{
    public class Movable
    {
        public Vector3 Scale = new Vector3(0.5f, 0.5f, 0.5f);
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();
        public Color color = new Color();
        public float mass = 1;
        public string objectType = " ";

        public float G = 0.05f;

        public void ApplyGravity(float scalar = 0.1f)
        {
            this.Acceleration += (new Vector3(0, -scalar * mass, 0) / mass);
        }

        public void ApplyForce(Vector3 force)
        {
            // F = MA
            // A = F/M
            this.Acceleration += (force / mass); //force accumulation
        }

        public virtual void Render(OpenGL gl)
        {
            gl.Color(color.r, color.g, color.b, color.a);
            UpdateMotion();
        }

        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            this.Acceleration *= 0;
        }

        public Vector3 CalculateAttraction(Movable movable)
        {
            var force = this.Position - movable.Position;
            var distance = force.GetLength();

            distance = OtherUtilities.Constrain(distance, 1, 5);

            force.Normalize();

            var strength = (this.G * this.mass * movable.mass) / (distance * distance);
            force *= strength;

            return force;
        }

        public void Repel(Movable target)
        {
            Vector3 repelForce = this.CalculateAttraction(target);

            target.ApplyForce((repelForce) * -1.0f);
        }

        public void Attract(Movable target)
        {
            Vector3 attractForce = this.CalculateAttraction(target);

            target.ApplyForce(attractForce);
        }

        public void Reflect(Movable target)
        {
            Vector3 location = target.Position - this.Position;

            this.Velocity -= location;
        }
    }
}
