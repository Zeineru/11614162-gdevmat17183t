﻿using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private Liquid ocean = new Liquid(0, 0, 100.0f, 50.0f, 0.9f);

        private List<Movable> shapeList = new List<Movable>();

        public Vector3 wind = new Vector3(5.0f, 0, 0);

        public Cube Jupiter = new Cube()
        {
            mass = 5,
            Scale = new Vector3(5, 5, 5)
        };

        public Cube Mercury = new Cube()
        {
            mass = 1,
            Position = new Vector3(15, 20, 0),
        };

        public Cube centerBody = new Cube()
        {
            mass = 10,
            Scale = new Vector3(10, 10, 10)
        };

        public MainWindow()
        {
            InitializeComponent();

            for (int i = 0; i < 20; i++)
            {
                createCubes(1);
            }

            for (int i = 0; i < 20; i++)
            {
                createCircles(1);
            }
        }

        #region INIZIALIZATION

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        void createCircles(float massAssigned)
        {
            Circle circleB = new Circle();

            circleB.color.r = (float)RandomNumberGenerator.GenerateDouble(0.0f, 2.0f);
            circleB.color.g = (float)RandomNumberGenerator.GenerateDouble(0.0f, 2.0f);
            circleB.color.b = (float)RandomNumberGenerator.GenerateDouble(0.0f, 2.0f);
            circleB.color.a = 100.0f;

            //circleB.mass = massAssigned;
            circleB.mass = RandomNumberGenerator.GenerateFloat(1.0f, 5.0f);

            circleB.Position.x = RandomNumberGenerator.GenerateFloat(-1.0f, -40.0f);
            circleB.Position.y = RandomNumberGenerator.GenerateFloat(1.0f, 40.0f);

            circleB.radius = circleB.mass / 2;

            shapeList.Add(circleB);
        }

        void createCubes(float massAssigned)
        {
            Cube cubeD = new Cube();

            cubeD.color.r = (float)RandomNumberGenerator.GenerateDouble(0.0f, 2.0f);
            cubeD.color.g = (float)RandomNumberGenerator.GenerateDouble(0.0f, 2.0f);
            cubeD.color.b = (float)RandomNumberGenerator.GenerateDouble(0.0f, 2.0f);
            cubeD.color.a = 100.0f;
            //cubeD.color.a = 0.0f;

            cubeD.mass = RandomNumberGenerator.GenerateFloat(1.0f, 5.0f);

            cubeD.Position.x = RandomNumberGenerator.GenerateFloat(-1.0f, -40.0f);
            cubeD.Position.y = RandomNumberGenerator.GenerateFloat(1.0f, 40.0f);

            shapeList.Add(cubeD);
        }

        void checkBounds(Movable shape)
        {
            if (shape.Position.x <= -30)
            {
                shape.Position.x = -30;
                shape.Velocity.x *= -1;
            }

            if (shape.Position.x >= 30)
            {
                shape.Position.x = 30;
                shape.Velocity.x *= -1;
            }

            if (shape.Position.y <= -40)
            {
                shape.Position.y = -40;
                shape.Velocity.y *= -1;
            }

            if (shape.Position.y >= 40)
            {
                shape.Position.y = 40;
                shape.Velocity.y *= -1;
            }
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            #endregion

            //Jupiter.Render(gl);
            //Mercury.Render(gl);

            //Mercury.ApplyForce(Jupiter.CalculateAttraction(Mercury));

            //checkBounds(Mercury);

            //centerBody.ApplyForce(wind);
            //centerBody.Render(gl);

            foreach (var c in shapeList)
            {
                c.ApplyForce(centerBody.CalculateAttraction(c));
                centerBody.ApplyForce(c.CalculateAttraction(centerBody));

                c.Render(gl);

                /*foreach (var d in shapeList)
                {
                    d.ApplyForce(d.CalculateAttraction(c));

                    d.Render(gl);
                }*/

                checkBounds(c);
            }

            //checkBounds(centerBody);
        }

        #region MOUSE FUNCTIONS
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
        }
        #endregion
    }
}
