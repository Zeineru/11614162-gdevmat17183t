﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Models
{
    public abstract class Movable
    {
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();
        public float mass = 1.0f;

        public float posX;
        public float posY;
        public double red = 1;
        public double blue = 1;
        public double green = 1;
        public double alpha = 1;

        public void ApplyForce(Vector3 force)
        {
            this.Acceleration += (force / mass);
        }

        public abstract void Render(OpenGL gl);

    }
}
