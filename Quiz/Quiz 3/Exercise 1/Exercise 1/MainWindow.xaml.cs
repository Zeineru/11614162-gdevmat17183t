﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exercise_1.Utilities;
using Exercise_1.Models;

namespace Exercise_1
{
    /// <summary>
    /// Interaction logic for MainWindow.
    /// </summary>

    public partial class MainWindow : Window
    {
        public int FRAMES = 0;

        private Vector3 mousePosition = new Vector3();

        public Vector3 direction = new Vector3(1, 1, 0);

        private Cube cubeA = new Cube();
        private Circle circleB = new Circle();

        private Vector3 wind = new Vector3(0.15f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.94f, 0);
        private Vector3 helium = new Vector3(0, 0.1f, 0);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 7";

            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);

            #endregion

            cubeA.Render(gl);
            cubeA.ApplyForce(wind);
            cubeA.ApplyForce(gravity);
            cubeA.red = 0.0f;
            cubeA.green = 110.0f;
            cubeA.blue = 57.0f;
            cubeA.alpha = 1;
            cubeA.mass = 1;

            if (cubeA.Position.x < -40)
            {
                cubeA.Velocity.x *= 40;
                cubeA.Position.x *= -1;
            }

            if (cubeA.Position.x > 40)
            {
                cubeA.Velocity.x *= -40;
                cubeA.Position.x *= -1;
            }

            if (cubeA.Position.y < -30)
            {
                cubeA.Position.y *= 30;
                cubeA.Velocity.y *= -1;
            }

            if (cubeA.Position.y > 30)
            {
                cubeA.Position.y *= -30;
                cubeA.Velocity.y *= -1;
            }

            circleB.Render(gl);
            circleB.ApplyForce(wind);
            circleB.ApplyForce(helium);
            circleB.red = 15.0f;
            circleB.green = 113.0f;
            circleB.blue = 98.0f;
            circleB.alpha = 1;
            circleB.mass = 20;

            if (circleB.Position.x < -40)
            {
                circleB.Velocity.x *= -1;
                circleB.Position.x *= -1;
            }

            if (circleB.Position.x > 40)
            {
                circleB.Velocity.x *= -1;
                circleB.Position.x *= -1;
            }

            if (circleB.Position.y < -30)
            {
                circleB.Position.y *= -1;
                circleB.Velocity.y *= -1;
            }

            if (circleB.Position.y < 30)
            {
                circleB.Position.y *= -1;
                circleB.Velocity.y *= -1;
            }

            FRAMES++;

            if(FRAMES >= 300)
            {
                cubeA.Position.x = 0;
                cubeA.Position.y = 0;

                cubeA.Position.x = 0;
                cubeA.Position.y = 0;
            }
        }

        #region INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };
            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);

            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);

            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.Enable(OpenGL.GL_BLEND);

            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        #region MOUSE MOVEMENT

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var positon = e.GetPosition(this);
            mousePosition.x = (float)positon.X - (float)Width / 2.0f;
            mousePosition.y = -((float)positon.Y - (float)Height / 2.0f);

            mousePosition.Normalize();
        }

        #endregion

    }
 }