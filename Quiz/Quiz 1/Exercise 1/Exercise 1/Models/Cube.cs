﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Models
{
    public class Cube : Movable
    {
        public Colors colors = new Colors();

        public Cube(float x = 0, float y = 0)
        {

        }

        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);

            gl.Begin(OpenGL.GL_QUADS);

            #region FRONT FACE

            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);

            #endregion

            gl.End();
        }
    }
}
