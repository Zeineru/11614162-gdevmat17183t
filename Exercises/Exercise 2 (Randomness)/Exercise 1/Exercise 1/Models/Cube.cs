﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Models
{
    public class Cube
    {
        public float posX;
        public float posY;

        public Colors colors = new Colors();

        public Cube(float x = 0, float y = 0)
        {

        }

        public void Render(OpenGL gl)
        {
            gl.Begin(OpenGL.GL_QUADS);

            gl.Color(colors.red, colors.green, colors.blue);

            #region FRONT FACE

            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);

            #endregion

            gl.End();
        }
    }
}
