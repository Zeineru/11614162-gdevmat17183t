﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Utilities
{
    public class RandomNumberGenerator
    {
        private static readonly Random random = new Random();

        public static int GenerateInt(int min, int max)
        {
            return random.Next(min, max);
        }

        public static double GenerateDouble(double min, double max)
        {
            return random.NextDouble() * (max - min) + min;
        }
    }
}
