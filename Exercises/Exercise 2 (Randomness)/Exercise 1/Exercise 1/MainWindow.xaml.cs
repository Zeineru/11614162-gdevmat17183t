﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exercise_1.Utilities;
using Exercise_1.Models;

namespace Exercise_1
{
    /// <summary>
    /// Interaction logic for MainWindow.
    /// </summary>

    enum Directions
    {
        Stay = 0,
        North = 1,
        South = 2,
        East = 3,
        West = 4,
        NorthEast = 5,
        NorthWest = 6,
        SouthEast = 7,
        SouthWest = 8
    };

    public partial class MainWindow : Window
    {
        const int HEADS = 0;
        const int TAILS = 1;

        private Cube cubeA = new Cube(-5, 3);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 2";

            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            #endregion

            cubeA.Render(gl);

            //gl.DrawText(0, 0, 1, 1, 1, "Arial", RandomNumberGenerator.GenerateInt(0, 100) + "");

            //int outcome = RandomNumberGenerator.GenerateInt(0, 8);
            int outcome = 0;

            switch (outcome)
            {
                case 0:
                {
                   
                }

                break;

                case 1:
                {
                    cubeA.posY++;
                }

                break;

                case 2:
                {
                    cubeA.posY--;
                 }

                break;

                case 3:
                {
                    cubeA.posX++;
                }

                break;

                case 4:
                {
                   cubeA.posX--;
                }

                break;

                case 5:
                {
                   cubeA.posY++;
                   cubeA.posX++;
                }

                break;

                case 6:
                {
                    cubeA.posY++;
                    cubeA.posX--;
                }

                break;

                case 7:
                {
                    cubeA.posY--;
                    cubeA.posX++;
                }

                break;

                case 8:
                {
                    cubeA.posY--;
                    cubeA.posX--;
                }

                break;
            }

            //cubeA.colors = new Models.Colors(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1));

            double otherOutcome = RandomNumberGenerator.GenerateDouble(0, 1);

            if (otherOutcome <= 0.2)
            {
                cubeA.posY++;
            }

            else if (otherOutcome <= 0.4)
            {
                cubeA.posX--;
            }

            else if (otherOutcome <= 0.6)
            {
                cubeA.posY--;
            }

            else
            {
                cubeA.posX++;
            }
        }

        #region INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
