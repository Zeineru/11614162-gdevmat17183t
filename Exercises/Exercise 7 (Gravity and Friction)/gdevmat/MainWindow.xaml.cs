﻿using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        //private Cube cubeA = new Cube();
        //private Circle circleB = new Circle();

        private Vector3 gravity = new Vector3(0, -0.5f, 0);
        private Vector3 wind = new Vector3(0.05f, 0, 0);

        private List<Circle> circleList = new List<Circle>();
        private List<Cube> cubeList = new List<Cube>();
        private List<Movable> shapeList = new List<Movable>();

        public int frames = 1;

        public MainWindow()
        {
            InitializeComponent();

            for (int i = 0; i < 10; i++)
            {
                createCircles(i);
            }
        }

        #region INIZIALIZATION

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        void createCircles(int massAssigned)
        {
            Circle circleB = new Circle();

            circleB.color.r = RandomNumberGenerator.GenerateFloat(0.1f, 1.0f);
            circleB.color.g = RandomNumberGenerator.GenerateFloat(0.1f, 1.0f);
            circleB.color.b = RandomNumberGenerator.GenerateFloat(0.1f, 1.0f);
            circleB.color.a = RandomNumberGenerator.GenerateFloat(0, 1.0f);

            circleB.mass = massAssigned;

            circleB.Position.x = -10;
            circleB.Position.y = 30;

            circleB.radius = circleB.mass / 2;

            shapeList.Add(circleB);
        }

        void checkBounds(List<Movable> shapeList, int index)
        {
            /*var coefficient = 0.01f;
            var normal = 1;
            var frictionMagnitude = coefficient * normal;
            var friction = shapeList[index].Velocity;

            friction *= -1;
            friction.Normalize();
            friction *= frictionMagnitude;

            shapeList[i].ApplyForce(friction);*/

            if (shapeList[index].Position.x <= -40)
            {
                shapeList[index].Velocity.x *= -1;
            }

            if (shapeList[index].Position.x >= 40)
            {
                shapeList[index].Velocity.x *= -1;
            }

            if (shapeList[index].Position.y <= -30)
            {
                shapeList[index].Velocity.y *= -1;
            }

            if (shapeList[index].Position.y >= 30)
            {
                shapeList[index].Velocity.y *= -1;
            }
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            #endregion

            for (int i = 0; i < shapeList.Count; i++)
            {
                var coefficient = 0.01f;
                var normal = 1;
                var frictionMagnitude = coefficient * normal;
                var friction = shapeList[i].Velocity;

                friction *= -1;
                friction.Normalize();
                friction *= frictionMagnitude;

                shapeList[i].ApplyForce(friction);

                shapeList[i].ApplyForce(gravity);
                shapeList[i].ApplyForce(wind);
                //shapeList[i].ApplyGravity(0.05f);
                shapeList[i].ApplyForce(friction);

                checkBounds(shapeList, i);

                shapeList[i].Render(gl);
            }
        }

        #region MOUSE FUNCTIONS
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
        }
        #endregion
    }
}
