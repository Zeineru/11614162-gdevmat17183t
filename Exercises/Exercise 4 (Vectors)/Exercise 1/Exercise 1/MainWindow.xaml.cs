﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exercise_1.Utilities;
using Exercise_1.Models;

namespace Exercise_1
{
    /// <summary>
    /// Interaction logic for MainWindow.
    /// </summary>

    public partial class MainWindow : Window
    {
        public Cube CubeB = new Cube(new Vector3(-5, 3, 0));

        public Vector3 direction = new Vector3(1, 1, 0);

        public MainWindow()
        {
            InitializeComponent();

            direction *= 2;
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 5";

            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            #endregion

            /*CubeB.Position += direction;

            if (CubeB.Position.x >= 30)
            {
                direction.x = -1;
            }

            else if (CubeB.Position.x <= -30)
            {
                direction.x = 1;
            }

            if (CubeB.Position.y >= 15)
            {
                direction.y = -1;
            }

            else if (CubeB.Position.y <= -15)
            {
                direction.y = 1;
            }

            CubeB.Render(gl);*/

            gl.LineWidth(5);
            gl.Color(1.0f, 1.0f, 1.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(15, 15);
            gl.Vertex(direction.x, direction.y);
            gl.End();

            gl.LineWidth(15);
            gl.Color(250.0f, 250.0f, 0.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(15, 15);
            gl.Vertex(direction.x, direction.y);
            gl.End();

            direction.Normalize();

            gl.DrawText(0, 0, 1, 1, 1, "Arial", 20, "Magnitude = " + direction.GetMagnitude().ToString());

        }

        #region INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };
            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);

            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);

            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.Enable(OpenGL.GL_BLEND);

            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion
    }
}