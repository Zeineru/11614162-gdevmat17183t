﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Models
{
    public abstract class Movable
    {
        public Vector3 Position = new Vector3(0, 0, 0);

        public float posX;
        public float posY;
        public double red = 1;
        public double blue = 1;
        public double green = 1;
        public double alpha = 1;

        public abstract void Render(OpenGL gl);

    }
}
