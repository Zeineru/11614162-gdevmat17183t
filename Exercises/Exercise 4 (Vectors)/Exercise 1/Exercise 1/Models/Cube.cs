﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Models
{
    public class Cube : Movable
    {
        public Colors colors = new Colors();

        public Cube(float x, float y, float z)
        {
            this.Position.x = x;
            this.Position.y = y;
            this.Position.z = z;
        }

        public Cube(Vector3 initPos)
        {
            this.Position = initPos;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);

            gl.Begin(OpenGL.GL_QUADS);

            #region FRONT FACE

            gl.Vertex(this.Position.x - 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y - 0.5f);
            gl.Vertex(this.Position.x - 0.5f, this.Position.y - 0.5f);

            #endregion

            gl.End();
        }
    }
}
