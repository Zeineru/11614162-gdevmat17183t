﻿using SharpGL;
using SharpGL.SceneGraph.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exercise_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /*float rotation = 0;*/

        private const float LINE_SMOOTHNESS = 0.3f;
        private const float GRAPH_LIMIT = 15;
        private const int TOTAL_CIRCLE_ANGLE = 360;
        float FREQUENCY = 2;
        float AMPLITUDE = 1;
        float TIME = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 1";

            #region DRAWING FUNCTIONS
            OpenGL gl = args.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);
            #endregion

            /*gl.PointSize(3.0f);
            gl.Begin(OpenGL.GL_POINTS);
            gl.Vertex(0, 0);
            gl.End();

            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(-15, 0);
            gl.Vertex(15, 0);
            gl.End();*/

            /*gl.Begin(OpenGL.GL_POINTS);

            for (float x = -(GRAPH_LIMIT - 5); x <= GRAPH_LIMIT - 5; x+= LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (2 * x) + 3);
            }

            gl.End();*/

            /*gl.Begin(OpenGL.GL_POINTS);

            for (float x = -(GRAPH_LIMIT - 5); x <= GRAPH_LIMIT - 5; x += LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (x * x) + (3 * x) - 4);
            }

            gl.End();*/

            /*float radius = 13.0f;

            gl.PointSize(1.0f);

            gl.Begin(OpenGL.GL_POINTS);

            for (float i = 0; i <= TOTAL_CIRCLE_ANGLE; i+=LINE_SMOOTHNESS)
            {
                gl.Vertex(Math.Cos(i) * radius, Math.Sin(i) * radius);
            }

            gl.End();*/

            gl.Begin(OpenGL.GL_POINTS);

            for (double x = -10; x <= 10; x+= LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (AMPLITUDE * Math.Sin(FREQUENCY * x + TIME)));


            }
            gl.End();

            TIME++;

            if (TIME >= 360)
            {
                TIME = 0;
            }

            if(Keyboard.IsKeyDown(Key.W))
            {
                AMPLITUDE++;
            }

            if(Keyboard.IsKeyDown(Key.S))
            {
                AMPLITUDE--;
            }

            if (Keyboard.IsKeyDown(Key.A))
            {
                FREQUENCY--;
            }

            if (Keyboard.IsKeyDown(Key.D))
            {
                FREQUENCY++;
            }

        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
