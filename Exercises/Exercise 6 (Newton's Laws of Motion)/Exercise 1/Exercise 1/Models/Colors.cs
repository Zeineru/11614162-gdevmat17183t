﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1.Models
{
    public class Colors
    {
        public double red;
        public double blue;
        public double green;
        public double alpha = 1;

        public Colors(double r = 1, double g = 1, double b = 1)
        {
            this.red = r;
            this.green = g;
            this.blue = b;
        }
    }
}
