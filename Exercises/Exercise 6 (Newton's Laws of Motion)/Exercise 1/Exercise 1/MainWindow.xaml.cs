﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exercise_1.Utilities;
using Exercise_1.Models;

namespace Exercise_1
{
    /// <summary>
    /// Interaction logic for MainWindow.
    /// </summary>

    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        public Vector3 direction = new Vector3(1, 1, 0);

        private Cube cubeA = new Cube();
        private Cube cubeB = new Cube();
        private Cube cubeC = new Cube();
        private Cube cubeD = new Cube();
        private Cube cubeE = new Cube();

        private Vector3 wind = new Vector3(1.0f, 0, 0);
        private Vector3 gravity = new Vector3(0, -9.40f, 0);
        private Vector3 push = new Vector3(0.50f, 0, 0);
        private Vector3 pull = new Vector3(-0.20f, 0, 0);
        private Vector3 normalForce = new Vector3(0, 9.40f, 0);

        public void checkX (Cube cube)
        {
            if (cubeA.Position.x >= 30)
            {
                int multiplier = -1;

                cubeA.Velocity *= multiplier;

                if (cubeA.Position.x <= -30)
                {
                    cubeA.Velocity *= multiplier;
                    cubeA.Position.x = 30;
                }
            }
        }

        public void checkY(Cube cube)
        {
            if (cubeA.Position.y >= 40)
            {
                int multiplier = -1;

                cubeA.Velocity *= multiplier;

                if (cubeA.Position.y <= -40)
                {
                    cubeA.Velocity *= multiplier;
                    cubeA.Position.y = 40;
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 7";

            #region DRAWING FUNCTIONS

            OpenGL gl = args.OpenGL;
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);

            #endregion

            cubeA.Render(gl);
            cubeA.ApplyForce(wind);
            checkX(cubeA);
            checkY(cubeA);
            cubeA.red = 256.0f;
            cubeA.green = 0.0f;
            cubeA.blue = 0.0f;
            cubeA.alpha = 1;

            cubeB.Render(gl);
            cubeB.ApplyForce(gravity);
            checkX(cubeB);
            checkY(cubeB);
            cubeB.red = 0.0f;
            cubeB.green = 256.0f;
            cubeB.blue = 0.0f;
            cubeB.alpha = 1;

            cubeC.Render(gl);
            cubeC.ApplyForce(push);
            checkX(cubeC);
            checkY(cubeC);
            cubeC.red = 0.0f;
            cubeC.green = 0.0f;
            cubeC.blue = 256.0f;
            cubeC.alpha = 1;

            cubeD.Render(gl);
            cubeD.ApplyForce(pull);
            checkX(cubeD);
            checkY(cubeD);
            cubeD.red = 256.0f;
            cubeD.green = 0.0f;
            cubeD.blue = 123.0f;
            cubeD.alpha = 1;

            cubeE.Render(gl);
            cubeE.ApplyForce(normalForce);
            checkX(cubeE);
            checkY(cubeE);
            cubeE.red = 256.0f;
            cubeE.green = 256.0f;
            cubeE.blue = 0.0f;
            cubeE.alpha = 1;
        }

        #region INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };
            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);

            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);

            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.Enable(OpenGL.GL_BLEND);

            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        #region MOUSE MOVEMENT

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var positon = e.GetPosition(this);
            mousePosition.x = (float)positon.X - (float)Width / 2.0f;
            mousePosition.y = -((float)positon.Y - (float)Height / 2.0f);

            mousePosition.Normalize();
        }

        #endregion

    }
 }