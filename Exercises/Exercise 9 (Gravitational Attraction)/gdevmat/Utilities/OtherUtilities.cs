﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat.Utilities
{
    public class OtherUtilities
    {
        public static float Constrain(float value, float min, float max)
        {
            if (value <= min)
            {
                return value;
            }

            else if (value >= max)
            {
                return value;
            }

            else
            {
                return value;
            }
        }
    }
}
